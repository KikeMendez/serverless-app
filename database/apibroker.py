from database.bootstrap import Database
from flask import request
import json
import datetime
import logging
import pymysql.cursors

class Apibroker():
	def __init__(self):
		self.username = request.environ['event']['stageVariables']['apibUsername']
		self.password = request.environ['event']['stageVariables']['apibPassword']
		self.host = request.environ['event']['stageVariables']['apibHost']
		self.db_name = request.environ['event']['stageVariables']['apibName']
		self.db_port = request.environ['event']['stageVariables']['apibPort']

	def connect(self):
		"""
		Connect to Apibroker
		"""
		apibroker = Database(
			self.host,
			self.username,
			self.password,
			self.db_name,
			self.db_port)
		return apibroker.connect()
