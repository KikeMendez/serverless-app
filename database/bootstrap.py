import logging
import pymysql
import werkzeug

from pymysql.err import IntegrityError

logging.basicConfig(level=logging.INFO,format="%(asctime)s %(levelname)s: %(message)s",datefmt="%Y-%m-%d %H:%M:%S")
logger = logging.getLogger()

class Database:
	"""
	Set database connection
	"""
	def __init__(self, host, username, password, db_name, db_port):
		 self.host = host
		 self.username = username
		 self.password = password
		 self.db_name = db_name
		 self.db_port = int(db_port)

	def connect(self):
		try:
			connection = pymysql.connect(
					host=self.host,
					user=self.username,
					passwd=self.password,
					db=self.db_name,
					port=self.db_port,
					connect_timeout=5)
		except Exception as an_exc:
			logger.error("Could not connect to MySql DB: {}".format(an_exc))
			raise
		return connection