from database.bootstrap import Database
from flask import request
from pymysql.err import IntegrityError
import pymysql
import logging
import werkzeug


logging.basicConfig(level=logging.INFO,format="%(asctime)s %(levelname)s: %(message)s",datefmt="%Y-%m-%d %H:%M:%S")
logger = logging.getLogger()

class Cdb:
	"""
	Initialize Cdb connection parameters
	"""
	def __init__(self):
		self.username = request.environ['API_GATEWAY_AUTHORIZER']['cdbUsername']
		self.password = request.environ['API_GATEWAY_AUTHORIZER']['cdbPassword']
		self.host = request.environ['API_GATEWAY_AUTHORIZER']['cdbHost']
		self.db_name = request.environ['API_GATEWAY_AUTHORIZER']['cdbName']
		self.db_port = request.environ['API_GATEWAY_AUTHORIZER']['cdbPort']

	def connect(self):
		"""
		Connect to Cdb
		"""
		cdb = Database(
			self.host,
			self.username,
			self.password,
			self.db_name,
			self.db_port)
		return cdb.connect()
