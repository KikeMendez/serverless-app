from database.apibroker import Apibroker
from helpers.output import decorate_response
from helpers.sql_result_to_json import create_pretty_dicts_list

def get_domains():
	apibroker = Apibroker()
	connectoToApiBroker = apibroker.connect()

	result = {"body": {"message": "ok", "data": {}}, "statusCode": 200}
	fields_to_return = ["id","host","product"]
	connection = connectoToApiBroker
	with connection.cursor() as cursor:
		sql	= "SELECT * FROM `domains`"
		cursor.execute(sql)
		items = cursor.fetchall()
		connection.close()
	dicted_items = create_pretty_dicts_list(fields_to_return, items)
	result["body"]["data"]["items"] = dicted_items
	return decorate_response(result)
