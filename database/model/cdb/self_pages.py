from database.cdb import Cdb
from helpers.output import decorate_response
from helpers.sql_result_to_json import create_pretty_dicts_list

def get_self_pages():
	cdb = Cdb()
	connectToCdb = cdb.connect()
	result = {"body": {"message": "ok", "data": {}}, "statusCode": 200}
	fields_to_return = ["id","name","type","title"]
	connection = connectToCdb
	with connection.cursor() as cursor:
		sql	= "SELECT * FROM `self_pages`"
		cursor.execute(sql)
		items = cursor.fetchall()
		connection.close()
	dicted_items = create_pretty_dicts_list(fields_to_return, items)
	result["body"]["data"]["items"] = dicted_items
	return decorate_response(result)


