from database.bootstrap import Database
from flask import request
import json
import datetime
import logging
import pymysql.cursors

class Xref():
	def __init__(self):
		self.username = request.environ['event']['stageVariables']['xrefUsername']
		self.password = request.environ['event']['stageVariables']['xrefPassword']
		self.host = request.environ['event']['stageVariables']['xrefHost']
		self.db_name = request.environ['event']['stageVariables']['xrefName']
		self.db_port = request.environ['event']['stageVariables']['xrefPort']

	def connect(self):
		"""
		Connect to xref
		"""
		xref = Database(
			self.host,
			self.username,
			self.password,
			self.db_name,
			self.db_port)
		return xref.connect()
