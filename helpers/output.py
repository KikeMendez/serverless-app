import datetime
import json
from flask import Response

def decorate_response(result):
    """
    Dump body as json, convert to Flask Response, add CORS headers.
    """
    result["body"] = json.dumps(result["body"], default=json_serialize_date)
    real_result = Response(response=result["body"],
                           status=result["statusCode"],
                           content_type="application/json")
    # Adding CORS headers.
    real_result.headers["Access-Control-Allow-Origin"] = "*"
    real_result.headers["Access-Control-Allow-Credentials"] = True
    return real_result

def json_serialize_date(obj):
    """
    JSON serializer for objects not serializable by default json code.
    Serializing datetime without it will raise exceptions.
    """
    if isinstance(obj, (datetime.datetime, datetime.date)):
        return obj.isoformat()
    raise TypeError("Type %s not serializable" % type(obj))