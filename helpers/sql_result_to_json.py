def create_pretty_dicts_list(fields, items):
    """
    Creates json from mysql select result.
    """
    result_list = []
    for item in items:
        item_dict = {}
        for i, field_name in enumerate(fields):
            item_dict[field_name] = item[i]
        result_list.append(item_dict)
    return result_list