#!/usr/bin/env python3

"""Routing requests by method inside an endpoint."""

from flask import Flask, request
from database.model.cdb import self_pages
from database.model.apibroker import domains
import json
app = Flask(__name__)

@app.route('/test', methods=['GET'])
def test_get():
    # getcdbConnectionDetails(request)
    # return process_api_call("exempt_processes", exempt_processes.get)
    # result = testApiBroker()
    result =  domains.get_domains()
    # result = self_pages.get_self_pages()
    return result

# @app.route('/compliance/exempt_processes', methods=['POST'])
# def exempt_processes_post():
#     getcdbConnectionDetails(request)
#     return process_api_call("exempt_processes", exempt_processes.post)


# @app.route('/compliance/exempt_processes', methods=['DELETE'])
# def exempt_processes_delete():
#     getcdbConnectionDetails(request)
#     return process_api_call("exempt_processes", exempt_processes.delete)

# @app.route('/compliance/exempt_processes', methods=['PUT'])
# def exempt_processes_put():
#     getcdbConnectionDetails(request)
#     return method_not_allowed(["GET", "POST", "DELETE"])
